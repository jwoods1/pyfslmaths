#!/usr/bin/env python

import os.path as op
import sys

import nibabel as nib

def multiply(data, arg):
    return data * arg


def subtract(data, arg):
    return data - arg


def add(data, arg):
    return data + arg


def binarise(data):
    return data > 0


def threshold(data, arg):
    data[data < arg] = 0
    return data


def dispatch(arg):
    if   arg == '-add':       return add,       1
    elif arg == '-subtract':  return subtract,  1
    elif arg == '-multiply':  return multiply,  1
    elif arg == '-bin':       return binarise,  0
    elif arg == '-threshold': return threshold, 1


def parse_argument(arg):
    if op.exists(arg):
        return nib.load(arg).get_fdata()
    else:
        return float(arg)

def main():

    infile  = sys.argv[1]
    args    = sys.argv[2:-1]
    outfile = sys.argv[-1]

    image = nib.load(infile)
    data  = image.get_fdata()
    i     = 0

    while i < len(args):
        op, nargs = dispatch(args[i])
        argstart  = i + 1
        argend    = i + 1 + nargs
        opargs    = [parse_argument(a) for a in args[argstart:argend]]
        data      = op(data, *opargs)
        i         = argend

    image = nib.Nifti1Image(data, None, image.header)
    image.to_filename(outfile)


if __name__ == '__main__':
    sys.exit(main())
